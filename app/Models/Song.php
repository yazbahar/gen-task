<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    use HasFactory;

    protected $table = 'song';

    protected $fillable = [
        'artist_id',
        'name',
        'genre'
    ];


    // Establishing a "belongsTo" relationship between the Song model and Artist model.
    // Selecting only the "id" and "name" columns when retrieving the related artist.
    public function artist()
    {
        return $this->belongsTo(Artist::class)->select(['id', 'name']);
    }
}
