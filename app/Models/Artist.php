<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    use HasFactory;

    protected $table = 'artist';

    protected $fillable = [
        'name'
    ];

    // Defining the relationship where an Artist has many Songs.
    public function songs()
    {
        return $this->hasMany(Song::class);
    }
}
