<?php

namespace App\Http\Controllers;

use App\Models\Song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    public function songs(Request $request)
    {
        /*
        $songs = Song::where('genre', 'Pop')->get();
        foreach ($songs as $song) {
            echo " Song: " . $song->name;
            echo " By Artist: " . $song->getArtist()->name;
            echo " (Artist has " . $song->getArtist()->total_songs . " total songs)";
            echo "\n";
        }
        **/

        // To retrieve songs along with their artists and the total number of songs for each artist in a single query,
        // you can utilize the Laravel "with" method as follows:
        $songs = Song::with(['artist' => function ($query) {
            return $query->withCount('songs as total_songs');
        }])
            ->where('genre', 'Pop')
            ->get();

        foreach ($songs as $song) {
            echo " Song: " . $song->name;
            echo " By Artist: " . $song->artist->name;
            echo " (Artist has " . $song->artist->total_songs . " total songs)";
            echo "\n";
        }
    }
}
