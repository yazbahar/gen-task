<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('song', function (Blueprint $table) {
            $table->id();
            $table->foreignId('artist_id')->constrained('artist');
            $table->string('name', 120); // Specifies a 'name' column with a maximum length of 120 characters. Length can be adjusted as needed.
            $table->string('genre', 100)->index(); // Specifies a 'genre' column with a maximum length of 100 characters and adds an index for efficient filtering by genre. Length can be adjusted as needed.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('song');
    }
};
